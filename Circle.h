﻿/*#pragma once
#include <iostream>
using namespace std;

class linia
{
private:
	static const int N = 100;
	int mas[N];
	int k;
	void add(int f);
public:
	linia() :k(0){};

	linia(int k0);

	linia(int k0, int mass[]);

	int getmas(int i)
	{
		return mas[i];
	}

	friend std::ostream& operator<<(std::ostream &, const linia &);

	friend std::istream& operator>>(istream&, linia  &);

	friend const linia operator +(const linia& g, const linia& r)
	{
		try{
			linia a(g);
			for (int i = 0; i < r.k && a.k < N; ++i)
			{
				a.add(r.mas[i]);
			}
		if (a.k<N)
			return a;
		else throw exception("vihod za predely massiva");
		}
		catch (exception){
			cout << "vihod za predel" << endl;
		}
	}

	linia operator += (int a)
	{
		k++;
		mas[k - 1] = a;
		return *this;
	}

	friend linia operator *(linia& b, linia& r)
	{
		try{
			linia r1(b), a;
			for (int i = 0; i < r1.k; ++i)
			{
				if (r.find(r1.mas[i]))
				{
					a.add(r1.mas[i]);
				}

			}
			return a;
		}
		catch (exception){
			cout << "vihod za predel" << endl;
		}
	}

		friend linia operator -(linia& b, linia& r)
		{
			try{
				linia g(b), a;
				for (int i = 0; i < g.k; ++i)
				{
					if (!r.find(g.mas[i]))
					{
						a.add(g.mas[i]);
					}
				}
				return a;
			}
			catch (exception){
				cout << "vihod za predel" << endl;
			}
		}

		int find(int f);
	};
	*/

#pragma once
#include <iostream>
using namespace std;

class linia
{
private:
	int *mas;
	int k;
	void add(int f);
public:
	linia()
	{
		k = 0;
		mas = NULL;
	}

	linia(int k0);

	linia(int k0, int mass[]);

	linia(const linia&);

	linia(linia && p);

	~linia()
	{
		delete[] mas;
	}

	int getmas(int i)
	{
		return mas[i];
	}

	friend std::ostream& operator<<(std::ostream &, const linia &);

	friend std::istream& operator>>(istream&, linia  &);

	friend linia operator +(const linia& r1, const linia& r)
	{
		linia a(r1);
		for (int i = 0; i < r.k; ++i)
		{
			a.add(r.mas[i]);
		}
		return a;
	}

	linia operator += (int a)
	{
		++k;
		int *q = mas;
		mas = new int[k];
		for (int i = 0; i <(k - 1); ++i)
		{
			mas[i] = q[i];
		}
		delete(q);
		mas[k - 1] = a;
		return *this;
	}

	linia& operator =(linia && a)
	{
		int t = k;
		k = a.k;
		a.k = t;
		int *p = mas;
		mas = a.mas;
		a.mas = p;
		return *this;
	}

	linia& operator =(const linia&a)
	{
		if (this != &a)
		{
			delete[] mas;
			mas = NULL;
			k = a.k;
			if (k != 0)
			{
				mas = new int[k];
				for (int i = 0; i < k; ++i)
				{
					mas[i] = a.mas[i];
				}
			}

		}
		return *this;
	}
	friend linia operator*(linia& a, linia & b)
	{
		int c = 0, j = 0;
		if (a.k < b.k)
			c = a.k;
		else
			c = b.k;
		int *mass = new int[c];
		for (int i = 0; i < a.k; ++i)
		{
			if (b.find(a.mas[i]))
			{
				mass[j] = a.mas[i];
				++j;
			}
		}
		linia m(j, mass);
		return m;
	}

	friend linia operator -(linia& b, linia& r)
	{
		linia g(b), a;
		for (int i = 0; i < g.k; ++i)
		{
			if (!r.find(g.mas[i]))
			{
				a.add(g.mas[i]);
			}
		}
		return a;
	}

	int find(int f);

	};