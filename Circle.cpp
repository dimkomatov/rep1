﻿/*#include "stdafx.h"
#include "Circle.h"
#include <iostream>

using namespace std;

linia::linia(int k0)
{
	k = k0;
	if (k<=N)
	{
		for (int i = 0; i < k0; ++i)
		{
			mas[i] = i;
		}
	} 
	 throw exception("n1");
}

linia::linia(int k0, int mass[N])
{
	k = 0;
		for (int i = 0; i < k0; ++i)
		{
			if (k<=N)
			add(mass[i]);
			else throw exception("povtorite vvod. vihod za predely massiva");
		}
	
}

int linia::find(int f)
{
	for (int i = 0; i < k; ++i)
	{
		if (mas[i] == f)
			return 1;
	}
	return 0;
}

istream & operator >> (istream & is, linia& v)
{
	int st;
	is >> v.k;
	int t;
	if (v.k > v.N)
	{
		is.setstate(std::ios::failbit);
		return is;
	}
	for (int i = 0; i < v.k; i++)
	{
		is >> t;
		if (!v.find(t))
			v.mas[i] = t;
		else
			--i;
	}
	return is;
}


ostream & operator << (ostream & os, const linia& v)
{
	os << "{";
	for (int i = 0; i<v.k; i++)
	{
		os << v.mas[i];
		if ((i + 1)<v.k)
			os << ",";
	}
	os << "}" << endl;
	return os;
}

void linia::add(int f)
{
	if (k<=N)
	{
		if (!find(f))
		{
			mas[k] = f;
			++k;
		}
	}
	else
		throw exception("vihod za predely massiva");
}
*/

#include "stdafx.h"
#include "Circle.h"
#include <iostream>

using namespace std;


linia::linia(int k0)
{
	k = k0;
	mas = new int[k0];
	for (int i = 0; i < k0; ++i)
	{
		mas[i] = i;
	}
}

linia::linia(int k0, int *mas)
{
	k = 0;
	for (int i = 0; i < k0; ++i)
	{
		add(mas[i]);
	}

}

linia::linia(const linia&a)
{
	k = a.k;
	mas = new int[k];
	for (int i = 0; i < k; ++i)
	{
		mas[i] = a.mas[i];
	}
}

linia::linia(linia && p)
{
	k = p.k;
	mas = p.mas;
	p.k = 0;
	p.mas = NULL;
}

int linia::find(int f)
{
	for (int i = 0; i < k; ++i)
	{
		if (mas[i] == f)
			return 1;
	}
	return 0;
}

istream & operator >> (istream & is, linia& v)
{
	int st;
	is >> v.k;
	v.mas = new int[v.k];
	int t;
	for (int i = 0; i < v.k; i++)
	{
		is >> t;
		if (!v.find(t))
			v.mas[i] = t;
		else
			--i;
	}
	return is;
}


ostream & operator << (ostream & os, const linia& v)
{
	os << "{";
	for (int i = 0; i<v.k; i++)
	{
		os << v.mas[i];
		if ((i + 1)<v.k)
			os << ",";
	}
	os << "}" << endl;
	return os;
}

/*void linia::add(int f)
{
if (!find(f))
{
mas[k] = f;
++k;
}
}*/

void linia::add(int f)
{
	if (!find(f))
	{
		int *a;
		a = new int[k + 1];
		for (int i = 0; i < k; ++i)
		{
			a[i] = mas[i];
		}
		a[k] = f;
		++k;
		delete[] mas;
		mas = a;
	}
}












