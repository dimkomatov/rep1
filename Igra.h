#pragma once
#include <stdio.h>
#include "Igrok.h"
#pragma once
using namespace std;
#include <string>
struct Date{
	int day, month, year;
	Date(int day = 0, int month = 0, int year = 0) :day(day), month(month), year(year){}
};
class Igra
{
protected:
	//Igrok **player;
	vector<Igrok> mas;

	Date date;
	string Sopern;
public:
	Igra() : player(NULL), kolvo(0), date(NULL), Sopern(NULL) {}
	Igra(Igrok** a, int k, int d, int m, int y, string s) : player(a), kolvo(k), Sopern(s), date (d,m,y) {}
	Igra(Igrok** a, int k, Date c, string d) : player(a), kolvo(k), date(c), Sopern(d) {}
	Date GetDay() const{ return date.day; }
	Date GetMounth() const{ return date.month; }
	Date GetYear() const{ return date.year; }
	void SetDate(Date a) { date = a; }
	string GetSopern(){ return Sopern; }
	string SetSopern(string s){ Sopern = s; }
	int Getkolvo() const{ return mas.size(); }
	Igrok* find(string a);
	int AddIgrok(string n, int t, int st);
	int DeleteIgrok(string n);
	Igrok* getPlayer()const;
	friend ostream& operator <<(std::ostream& stream, const Igra&);
	virtual ~Igra();
};

